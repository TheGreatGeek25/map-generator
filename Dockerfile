FROM node:18 as build-env
WORKDIR /designer

COPY . ./
RUN npm install
RUN npx lessc style.less public/style.css
RUN npx tsc

FROM nginx
WORKDIR /usr/share/nginx/html
COPY --from=build-env /designer/public ./
