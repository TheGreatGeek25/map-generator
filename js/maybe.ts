/**
 * Either `Just` a T or `Nothing`
 */

export interface Maybe<T> {
    /**
     * Chain into a function which returns a Maybe
     * If this is Nothing, ignore the function and return Nothing
     * If this is Just(some value) pass that value to the function and return the result
     */
    next: <U>(f: (x: T) => Maybe<U>) => Maybe<U>;

    /**
     * Maybe eliminator
     * If this is Nothing, return the second parameter
     * If this is Just(some value) pass that value to the first paramater and return the result
     */
    maybe: <U>(t: (x: T) => U, f: U) => U;

    /**
     * True if this is not Nothing
     */
    isJust(): this is { val: T; };
}
interface Just<T> extends Maybe<T> { val: T; }

export function Just<T>(val: T): Just<T> {
    return {
        val: val,
        next(f) { return f(this.val); },
        maybe(t, _) { return t(val); },
        isJust() { return true; }
    };
}

export const Nothing: Maybe<never> = {
    next(_) { return Nothing; },
    maybe(_, f) { return f; },
    isJust() { return false; }
};
