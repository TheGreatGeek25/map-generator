import type * as viewmodel from "viewmodel";
import * as serializer from "./serializer";
import * as pubsub from "pubsub";
import { regionToObj } from "./serializer";
import { Just, Maybe, Nothing } from "./maybe";

export async function deleteRegion(name: string, regionStore: URL) {
    return fetch(new URL(`region/${name}`, regionStore), {
        method: 'DELETE'
    });
}

async function postRegionJSON(region: string, regionStore: URL) {
    return fetch(new URL(`region`, regionStore), {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: region
    })
}

export async function postRegion(region: viewmodel.Region, regionStore: URL) {
    return postRegionJSON(JSON.stringify(regionToObj(region)), regionStore);
}

async function fetchJSON(url: URL): Promise<Maybe<unknown>> {
    let response = await fetch(url);
    if (response.status === 404) return Nothing;
    let reader = response.body?.getReader()
    if (reader === undefined) return Promise.reject("No response body");
    let decoder = new TextDecoder();
    let str = "";
    let done, value;
    while (true) {
        ({done, value} = await reader.read());
        if (done) {
            str += decoder.decode();
            try {
                return Just(JSON.parse(str));
            } catch (err) {
                return Promise.reject("Invalid JSON");
            }
        }

        str += decoder.decode(value, {stream: true});
    }
}

async function fetchRegionJSON(name: string, regionStore: URL): Promise<Maybe<unknown>> {
    return fetchJSON(new URL(`region/${name}`, regionStore));
}

export async function fetchRegion(name: string, tiles: viewmodel.Tile[], regionStore: URL): Promise<viewmodel.Region> {
    let json = await fetchRegionJSON(name, regionStore);
    if (json.isJust()) {
        if(typeof json.val === 'object' && json.val !== null) {
            let region = serializer.loadObjRegion(json.val, tiles);
            if (region.isJust()) {
                return Promise.resolve(region.val);
            } else {
                return Promise.reject("Object received is not a region")
            }
        } else return Promise.reject("Response is either Null or Not an object");
    } else {
        return Promise.reject("Region not in database")
    }
}

async function fetchIconSetJSON(name: string, iconSets: URL): Promise<Maybe<unknown>> {
    return fetchJSON(new URL(`${name}.json`, iconSets));
}

export async function fetchIconSet(name: string, iconSets: URL): Promise<viewmodel.Tile[]> {
    let json = await fetchIconSetJSON(name, iconSets);
    let out: viewmodel.Tile[] = [];
    if (json.isJust()) {
        if(!Array.isArray(json.val)) return Promise.reject("Object recieved is not an array");

        for(let t of json.val as unknown[]) {
            let mt = serializer.loadTile(t);
            if (mt.isJust()) out.push(mt.val);
            else return Promise.reject("Object is not a tile")
        }
        return out;
    } else { 
        return Promise.reject("Icon set not found");
    }
}

export const iconSets = new URL("https://map-generator.charlieom.net/icon-sets/");
export const regionStore = new URL("https://api.map-generator.charlieom.net:7283")
pubsub.exportRegionStore.subscribe((r) => postRegion(r, regionStore)
    .catch(reason => pubsub.netFailed.pub({method: 'post', resource: `region ${r.name()}`, reason})))
pubsub.deleteRegionStore.subscribe((r) => deleteRegion(r.name(), regionStore)
    .catch(reason => pubsub.netFailed.pub({method: 'delete', resource: `region ${r.name()}`, reason})));
pubsub.netFailed.subscribe(({method, resource, reason}) => alert(`Failed to ${method} ${resource}: ${reason}`))