import * as ko from "knockout";
import { Maybe, Just, Nothing } from "./maybe";

/** 
 * A random generator which generates either a T or nothing
 * Will retry until a result is found or there is nothing left to try
 */
export interface Generator<T> {
    /** 
     * Given an Generator<T> comp and a function foo which maps
     * T to Generator<U> (some some type U), compA.next(foo) is
     * a generator which is the result of chaining compA into foo as follows:
     *  * Assume that res is the result of running comp.
     *  * the result of running comp.next(foo) is the result of running foo(res)
     *  * if foo(res) has no result, we backtrack, find a new value of res, and try again
     *  * only if there is no value of res which would generate a result from foo(res)
     *    will comp.next(foo) fail to produce a value
     */
    next: <U> (f: (x: T) => Generator<U>) => Generator<U>;

    /**
     * Run the generator with a depth limit of `limit`
     * If it can generate a result, return {val: Just(the result), lim: newLimit}
     * Otherwise, return {val: Nothing, lim: newLimit}
     * Where newLimit is the new depth limit after the attempted generation
     */
    run: (limit: number) => {val: Maybe<T>, lim: number};
}

/**
 * A simple generator which always fails to produce a result
 */
export const generatorFail : Generator<never> = {
    next() { return generatorFail },
    run(limit) { return {val: Nothing, lim: limit-1} }
}

/**
 * A simple generator which always produces x
 */
export function generatorPure<T>(x: T): Generator<T> {
    return {
        next(g) { return g(x); },
        run(limit) { return {val: Just(x), lim: limit-1}; }
    }
}

/**
 * A chain of generators, such that the start of the chain accepts an S
 * as input, and at the end of the chain, a T is produced
 * 
 * This is encoded as a type of function which, given a function from T to
 * Generator<V> as a parameter, will return a function from S to Generator<V>
 * 
 * That is, a Chain<S, T> is a function which will chain its argument
 * after some unknown Generator<T> 
 * 
 * Note that this means that a properly typed identity function is a Chain<S, S> for any S
 */
interface Chain<S, T> {
    <V>(nf: (i: T) => Generator<V>): (start: S) => Generator<V>
}

/**
 * Add a generator function to a Chain.
 * Given a Chain<S,T> called `ch` and a generator function from T to Generator<U> called `act`,
 * this function will return a new Chain<S, U> which is the result connected the final output of `ch`
 * to the input of `act`
 */
function chain<T, U, S>(ch: Chain<S, T>, act: (a: T) => Generator<U>): Chain<S, U> {
    return <V>(nextAct: (i: U) => Generator<V>) => ch((a) => act(a).next(nextAct))
} 

/**
 * Creates a Generator<T[]> which randomly selects one of the `choices` as the generation result
 * If dropRest is true, and the Generator is forced to retry, it will never try an option which
 * after an option it has already tried in the list
 */
export function generatorChooseArr<T>(choices: T[], dropRest: boolean = false): Generator<T> {
    let id = <V>(f: (n: T) => Generator<V>) => f
    function chooseArrHelp<V>(ch: Chain<T, V>): Generator<V> {
        return {
            next(g) {
                return chooseArrHelp(chain(ch, g))
            },

            run(limit) {
                let r: Maybe<V> = Nothing;
                let c: T;
                while (!r.isJust() && choices.length > 0) {
                    if (limit <= 0) {return {val: Nothing, lim: limit};}
                    c = choicePop(choices, dropRest);
                    ({val: r, lim: limit} = ch(generatorPure)(c).run(limit-1));
                } 

                if (choices.length == 0 && !r.isJust()) {
                    return {val: Nothing, lim: limit};
                }
                return {val:r, lim: limit};
            }
        }
    }
    return chooseArrHelp(id)
}

/**
 * Creates a Generator<T[]> which randomly selects one of the `options` as the generation result
 * The likelyhood of selecting each options is given by its `weight` field
 * 
 * If the total weights of all options is less than one, the difference is a possibility
 * of selecting `Nothing` as an option, which can be a success result
 *  * in other words, a result of `Nothing` is not a failure to do something, it is success at not doing something
 */
export function generatorChooseWeighted<T extends { weight: number | KnockoutObservable<number> }>(options: T[]): Generator<Maybe<T>> {
    let id = <V>(f: (n: Maybe<T>) => Generator<V>) => f
    function chooseWeightedHelp<V>(ch: Chain<Maybe<T>,V>): Generator<V> {
        return {
            next(g) {
                return chooseWeightedHelp(chain(ch, g))
            },
            run(lim) {
                let scale = 1;
                let wtmp = options.slice();
                let result: Maybe<V> = Nothing;
                let chosenOpt: Maybe<T>;
                let allowNone = totalWeight(options) < 1;
                do {
                    if (lim <= 0) {return {val: Nothing, lim};}
                    chosenOpt = Nothing;
                    let n = Math.random();
                    let sum = 0;
                    
                    for (let opt of wtmp) {
                        if (n < sum + ko.unwrap(opt.weight)*scale) {
                            chosenOpt = Just(opt);
                            continue;
                        }
                        sum += ko.unwrap(opt.weight)*scale;
                    }
                    if (chosenOpt.isJust()) removeFromArr(wtmp, chosenOpt.val);
                    scale /= sum;
                    if (chosenOpt !== Nothing || allowNone) {
                        ({val: result, lim} = ch(generatorPure)(chosenOpt).run(lim-1));
                    }
                } while (!result.isJust() && !(chosenOpt === Nothing && wtmp.length === 0));
    
                return {val: result, lim};
            }
        }
    }
    return chooseWeightedHelp(id);
    
}

function totalWeight<T extends { weight: number | KnockoutObservable<number> }>(options: T[]): number {
    let sum = 0;
    for (let opt of options) {
        sum += ko.unwrap(opt.weight)
    }
    return sum;
}

export function removeFromArr<T>(arr: T[], elem: T): number {
    let iToRemove = arr.indexOf(elem);
    if (iToRemove != -1) arr.splice(iToRemove, 1);
    return iToRemove;
}

function choicePop<T>(arr: T[], dropRest: boolean = false): T {
    let i = randint(0, arr.length-1);
    if (dropRest) return arr.splice(i)[0];
    else return arr.splice(i,1)[0];
}

function randint(min: number, max: number): number {
    return Math.floor(Math.random() * (max-min+1)) + min
}