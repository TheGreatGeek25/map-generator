import * as ko from "knockout";
import * as svg from "svg";
import * as pubsub from "pubsub";
import {generate} from "generate";
import { Just, Maybe, Nothing } from "./maybe";
import * as network from "network";

export type RCCoords = {r: number, c: number};
export type Interval<T> = {min: T, max: T};
export type ObsRCCoords = {r: KnockoutObservable<number>, c: KnockoutObservable<number>};
export type ObsInterval<T> = {min: KnockoutObservable<T>, max: KnockoutObservable<T>};
export type TileMap = {[x: number]: {[y: number]: Tile}};
export type RegionMap = {[name: string]: Region};

export function unwrapRC(rc: ObsRCCoords | RCCoords): RCCoords {
    return { r: ko.unwrap(rc.r), c: ko.unwrap(rc.c) };
}

export function mapRegions(rs: Region[]): RegionMap {
    let rm: RegionMap = {};
    for (let r of rs) {
        rm[r.name()] = r;
    }
    return rm;
}

export class Tile {
    name: string;
    svg: string;

    selectTile: (tile: Tile) => void = (tile) => pubsub.selectTile.pub(tile);

    constructor(name: string, svg: string, scale = {x: 1, y: 1}, translate = {x: 0, y: 0}) {
        this.name = name;
        this.svg = `<svg id="tiles_${name}" width="4em" height="4em" viewBox="-0.5 -0.5 1 1" xmlns="http://www.w3.org/2000/svg"><g transform="translate(${translate.x} ${translate.y}) scale(${scale.x} ${scale.y})">${svg}</g></svg>`;
    }
}

export const blankTile = new Tile("blank", "");

function makeIconSetLoader(): () => void {
    let setNum = 0;
    return function() {
        pubsub.importIconSet.pub(setNum.toFixed().padStart(2, '0'));
        setNum++;
    }
}

export class Region {
    name: KnockoutObservable<string>;
    tile: KnockoutObservable<Tile> = ko.observable(blankTile);

    size: Interval<ObsRCCoords> = {
        min: {r: ko.observable(1).extend({number: "Minimum row size must be a number"}),
              c: ko.observable(1).extend({number: "Minimum columns size must be a number"})},
        max: {r: ko.observable(1).extend({number: "Maximum row size must be a number"}),
              c: ko.observable(1).extend({number: "Maximum columns size must be a number"})},
    };

    choices: KnockoutObservableArray<Choice> = ko.observableArray();
    
    addSubregionChoice: () => void = () => {
        pubsub.addSubregionChoice.pub(new SubregionChoice());
    };

    addCorridorChoice: () => void = () => {
        pubsub.addCorridorChoice.pub(new CorridorChoice())
    };

    editRegion: (region: Region) => void = (r) => pubsub.editRegion.pub(r);
    delRegion: (region: Region) => void = (r) => pubsub.delRegion.pub(r);

    dragregion: (region: Region, event: DragEvent) => boolean = (region, event) => {
        event.dataTransfer?.setData("text", region.name());
        let elems = document.getElementsByClassName("region-field");
        for (let i=0; i < elems.length; i++) {
            elems.item(i)!.classList.add("field-emph");
        }
        return true;
    }

    dragendregion: (_: unknown, event: DragEvent) => void = (_, event) => {
        let elems = document.getElementsByClassName("region-field");
        for (let i = 0; i < elems.length; i ++) {
            elems.item(i)?.classList.remove("field-emph"); 
        }
    }

    constructor(name: string) {
        this.name = ko.observable(name);

        pubsub.delChoice.subscribe((choice) => this.choices.remove(choice));
    };
        
}

export interface Choice {
    type: 'subregion' | 'corridor'
    options: KnockoutObservableArray<Subregion> | KnockoutObservableArray<Corridor>
    delChoice: (c: Choice) => void

    addOption(): void;
}

export class SubregionChoice implements Choice {
    type = 'subregion' as const;
    options: KnockoutObservableArray<Subregion> = ko.observableArray();
    delChoice = (c: Choice) => pubsub.delChoice.pub(c)
    addOption() {
        pubsub.addSubregion.pub({choice: this, option: new Subregion()});
    }
}

export class CorridorChoice implements Choice {
    type = 'corridor' as const;
    options: KnockoutObservableArray<Corridor> = ko.observableArray();
    delChoice = (c: Choice) => pubsub.delChoice.pub(c)
    addOption() {
        pubsub.addCorridor.pub({choice: this, option: new Corridor()});
    }
}

export class Subregion {
    weight: KnockoutObservable<number> = ko.observable(0).extend({number: "Weight must be a number"});
    region: KnockoutObservable<string> = ko.observable("");
    placement: KnockoutObservable<"at" | "randomly"> = ko.observable<"at" | "randomly">("randomly");
    atPos: ObsRCCoords = {r: ko.observable(0).extend({number: "Row position must be a number"}),
                          c: ko.observable(0).extend({number: "Column position must be a number"})};
    repeatRange: ObsInterval<number> = {min: ko.observable(1).extend({number: "Minimum repititions must be a number"}),
                                        max: ko.observable(1).extend({number: "Maximum repititions must be a number"})};

    delOption(c: SubregionChoice, r: Subregion) {
        pubsub.delSubregion.pub({choice: c, option: r});
    }
}

export class Corridor {
    weight: KnockoutObservable<number> = ko.observable(0).extend({number: "Weight must be a number"});
    doorRegion: KnockoutObservable<string> = ko.observable("");
    placement: KnockoutObservable<"at" | "randomly"> = ko.observable<"at" | "randomly">("randomly");
    path: KnockoutObservable<"random walk"> = ko.observable("random walk");
    segmentLen: ObsInterval<number> = {min: ko.observable(1).extend({number: "Minimum segment length must be a number"}),
                                       max: ko.observable(1).extend({number: "Maximum segment length must be a number"})};
    segmentNum: ObsInterval<number> = {min: ko.observable(1).extend({number: "Minimum segment number must be a number"}),
                                       max: ko.observable(1).extend({number: "Maximum segment number must be a number"})};
    atPos: ObsRCCoords = {r: ko.observable(0).extend({number: "Row position must be a number"})
                         ,c: ko.observable(0).extend({number: "Column position must be a number"})};
    endingRegion: KnockoutObservable<string> = ko.observable("");
    repeatRange: ObsInterval<number> = {min: ko.observable(1).extend({number: "Minimum repititions must be a number"}),
                                        max: ko.observable(1).extend({number: "Maximum repititions must be a number"})};
    pathTile: Tile = blankTile;

    delOption(c: CorridorChoice, r: Corridor) {
        pubsub.delCorridor.pub({choice: c, option: r});
    }
}

export class ViewModel {
    errors: KnockoutObservableArray<string> = ko.observableArray();
    selectedRegion: KnockoutObservable<Region | null> = ko.observable(null);

    regions: KnockoutObservableArray<Region> = ko.observableArray();
    tiles = ko.observableArray([
        blankTile,
        new Tile("big circle", '<circle r="0.45" stroke="black" stroke-width="0.05" fill="transparent"/>'),
        new Tile("small circle", '<circle r="0.2" stroke="black" stroke-width="0.05" fill="transparent"/>')
    ]);

    generatorOpen: KnockoutObservable<boolean> = ko.observable(false);
    importOpen: KnockoutObservable<boolean> = ko.observable(false);
    exportOpen: KnockoutObservable<boolean> = ko.observable(false);
    tileSelectorOpen: KnockoutObservable<boolean> = ko.observable(false);

    mapZoom: KnockoutObservable<number> = ko.observable(1);
    mapYOffset: KnockoutObservable<number> = ko.observable(-15);
    mapXOffset: KnockoutObservable<number> = ko.observable(-50);

    viewBox: KnockoutComputed<string> = ko.computed(() => {
        let z = this.mapZoom();
        return (this.mapXOffset()/z) + " " + (this.mapYOffset()/z) + " " + (100/z) + " " + (100/z);
    });

    mapDrag: KnockoutObservable<boolean> = ko.observable(false);
    mapMouseDown: () => void = () => {
        this.mapDrag(true);
    }
    mapMouseUp: () => void = () => {
        this.mapDrag(false);
    }
    mapMove: (_: unknown, event: PointerEvent) => void = (_, event) => {
        if (!this.mapDrag()) return;

        let ww = window.innerWidth;
        let wh = window.innerHeight;
        let propMx = event.movementX/ww;
        let propMy = event.movementY/wh;
        this.mapXOffset(this.mapXOffset() - propMx*100);
        this.mapYOffset(this.mapYOffset() - propMy*100);
    }

    zoomIn: () => void = () => pubsub.zoomIn.pub(1.5);
    zoomOut: () => void = () => pubsub.zoomOut.pub(1.5);

    addRegion: () => void = () => {
        pubsub.newRegion.pub(new Region((document.getElementById("newRegionName")! as HTMLInputElement).value));
    }

    getTile: (name: string) => Tile = (name) => {
        throw this.tiles().find((t) => t.name === name);
    }
    
    allowdrop: (_: unknown, event: DragEvent) => void = (_, event) => {
        event.preventDefault();
    }

    onregiondrop: (field: KnockoutObservable<string>) => (_: unknown, event: DragEvent) => void = (field) => {
        return function (_: unknown, event: DragEvent) {
            event.preventDefault();
            field(event.dataTransfer!.getData("text"));
        }
    }

    runGenerator: () => void = () => {
        let form = document.getElementById("generatorConfigForm") as HTMLFormElement;
        let region: string = form.generationRegion.value;
        let limit: number = form.depthLimit.valueAsNumber;
        pubsub.generateDungeon.pub({region: this.regions().find(e => e.name() === region)!, limit})
    }

    runImport: () => void = () => {
        let r = document.getElementById("importRegion") as HTMLInputElement;
        pubsub.importRegionStore.pub(r.value);
    }
    runExport: () => void = () => {
        let r = document.getElementById("exportRegion") as HTMLInputElement;
        let region = this.regions().find(e => e.name() === r.value);
        if (region !== undefined) {
            pubsub.exportRegionStore.pub(region);
        } else {
            alert("This region is not defined");
        }
    }

    loadMoreTiles: () => void = makeIconSetLoader();

    constructor() {
        pubsub.newRegion.subscribe((ev) => this.regions.push(ev));
        pubsub.editRegion.subscribe(this.selectedRegion);

        pubsub.delRegion.subscribe((ev) => {
            this.regions.remove(ev);
            if (this.selectedRegion() && this.selectedRegion()!.name() == ev.name()) {
                this.selectedRegion(null);
            }
        });

        pubsub.selectTile.subscribe((tile) => {
            this.selectedRegion()?.tile(tile);
            this.tileSelectorOpen(false);
        });

        pubsub.addSubregionChoice.subscribe((ev) => this.selectedRegion()?.choices.push(ev));
        pubsub.addCorridorChoice.subscribe((ev) => this.selectedRegion()?.choices.push(ev));

        pubsub.generateDungeon.subscribe(({region, limit}) => {
            svg.clearMap();
            let {val: result, lim} = generate(mapRegions(this.regions()), {}, region, {r: 0, c: 0}, {pos:{r:-50,c:-50},dim:{r:100,c:100}}).run(limit);
            if(result.isJust()) {
                svg.drawTiles(result.val);
            } else {
                alert(`Generation failed: impossible (at final limit ${lim}/${limit})`)
            }
        })
        
        pubsub.zoomIn.subscribe((zf) => this.mapZoom(this.mapZoom() * zf));
        pubsub.zoomOut.subscribe((zf) => this.mapZoom(this.mapZoom() / zf));

        pubsub.importRegionStore.subscribe((r) => network.fetchRegion(r, this.tiles(), network.regionStore)
            .then(region => pubsub.importRegionReceived.pub(region), reason => pubsub.netFailed.pub({method: 'get', resource: `region ${r}`, reason})));
        pubsub.importIconSet.subscribe((s) => network.fetchIconSet(s, network.iconSets).then(ts => this.tiles.push(...ts)))
    }
}

declare global {
    interface KnockoutExtenders {
        number(t: KnockoutObservable<number | string>, errorMsg: string): KnockoutObservable<number | string>;
    }
}


let viewModel: ViewModel;

ko.extenders.number = function(t, errorMsg) {
    function number(val: number | string) {
        let n = Number(val);
        viewModel.errors.remove(errorMsg);
        if (isNaN(n)) {
            viewModel.errors.push(errorMsg);
        } else {
            t(n);
        }
    }

    number(t());
    t.subscribe(number);

    return t;
}

ko.bindingHandlers.modalOpen = {
    update: function(elem, valAcc) {
        let val = ko.unwrap(valAcc());
        if (val) {
            elem.showModal();
        } else {
            elem.close();
        }
    }
}

pubsub.delSubregion.subscribe(({choice, option}) => choice.options.remove(option));
pubsub.addSubregion.subscribe(({choice, option}) => choice.options.push(option));
pubsub.delCorridor.subscribe(({choice, option}) => choice.options.remove(option));
pubsub.addCorridor.subscribe(({choice, option}) => choice.options.push(option));

pubsub.importRegionReceived.subscribe((r) => pubsub.newRegion.pub(r));

viewModel = new ViewModel();
ko.applyBindings(viewModel);