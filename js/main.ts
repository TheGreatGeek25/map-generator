import * as pubsub from "pubsub";
import "network";

// ==================================================================
//                         Load Testing Data
// ==================================================================

pubsub.importRegionStore.pub("Coin");
pubsub.importRegionStore.pub("Square");
pubsub.importRegionStore.pub("Room 1");
