import { Maybe, Just, Nothing } from "./maybe";
import { Subregion, Corridor, Choice, SubregionChoice, CorridorChoice, Tile, Region } from "./viewmodel";

export function regionToObj(r: Region): {[k: string]: unknown} {
    let out: {[k: string]: unknown} = {};
    out.name = r.name();
    out.tile = r.tile().name;
    out.sizeMinR = r.size.min.r();
    out.sizeMinC = r.size.min.c();
    out.sizeMaxR = r.size.max.r();
    out.sizeMaxC = r.size.max.c();
    out.choices = r.choices().map(choiceToObj);
    return out;
}

function choiceToObj(c: Choice): {[k: string]: unknown} {
    let out: {[k: string]: unknown} = {};
    out.type = c.type;
    if (c instanceof SubregionChoice) {
        out.options = c.options().map(subregionToObj);
    } else if (c instanceof CorridorChoice) {
        out.options = c.options().map(corridorToObj);
    } else {
        throw new Error("Invalid choice (this should be impossible)")
    }
    return out;
}

function subregionToObj(s: Subregion): {[k: string]: unknown} {
    let out: {[k: string]: unknown} = {};
    out.weight = s.weight();
    out.region = s.region();
    out.placement = s.placement();
    out.atPosR = s.atPos.r();
    out.atPosC = s.atPos.c();
    out.repeatRangeMin = s.repeatRange.min();
    out.repeatRangeMax = s.repeatRange.max();
    return out;
}

function corridorToObj(c: Corridor): {[k: string]: unknown} {
    let out: {[k: string]: unknown} = {};
    out.weight = c.weight();
    out.doorRegion = c.doorRegion();
    out.endingRegion = c.endingRegion();
    out.placement = c.placement();
    out.atPosR = c.atPos.r();
    out.atPosC = c.atPos.c();
    out.path = c.path();
    out.segmentLenMin = c.segmentLen.min();
    out.segmentLenMax = c.segmentLen.max();
    out.segmentNumMin = c.segmentNum.min();
    out.segmentNumMax = c.segmentNum.max();
    out.repeatRangeMin = c.repeatRange.min();
    out.repeatRangeMax = c.repeatRange.max();
    return out;
}

export function loadObjSubregion(obj: object): Maybe<Subregion> {
    let out = new Subregion();
    if ('weight' in obj && typeof obj.weight === 'number') out.weight(obj.weight);
    else return Nothing;

    if ('region' in obj && typeof obj.region === 'string') out.region(obj.region);
    else return Nothing;

    if ('placement' in obj && (obj.placement === 'at' || obj.placement === 'randomly')) out.placement(obj.placement);
    else return Nothing;

    if ('atPosR' in obj && typeof obj.atPosR === 'number'
        && 'atPosC' in obj && typeof obj.atPosC === 'number') {
        out.atPos.r(obj.atPosR);
        out.atPos.c(obj.atPosC);
    } else return Nothing;

    if ('repeatRangeMin' in obj && typeof obj.repeatRangeMin === 'number'
        && 'repeatRangeMax' in obj && typeof obj.repeatRangeMax === 'number') {
        out.repeatRange.min(obj.repeatRangeMin);
        out.repeatRange.max(obj.repeatRangeMax);
    } else return Nothing;

    return Just(out);
}

export function loadObjCorridor(obj: object): Maybe<Corridor> {
    let out = new Corridor();
    if ('weight' in obj && typeof obj.weight === 'number') out.weight(obj.weight);
    else return Nothing;

    if ('doorRegion' in obj && typeof obj.doorRegion === 'string') out.doorRegion(obj.doorRegion);
    else return Nothing;

    if ('endingRegion' in obj && typeof obj.endingRegion === 'string') out.endingRegion(obj.endingRegion);
    else return Nothing;

    if ('placement' in obj && (obj.placement === 'at' || obj.placement === 'randomly')) out.placement(obj.placement);
    else return Nothing;

    if ('path' in obj && obj.path === 'random walk') out.path(obj.path);
    else return Nothing;

    if ('segmentLenMin' in obj && typeof obj.segmentLenMin === 'number'
        && 'segmentLenMax' in obj && typeof obj.segmentLenMax === 'number') {
        out.segmentLen.min(obj.segmentLenMin);
        out.segmentLen.max(obj.segmentLenMax);
    } else return Nothing;

    if ('segmentNumMin' in obj && typeof obj.segmentNumMin === 'number'
        && 'segmentNumMax' in obj && typeof obj.segmentNumMax === 'number') {
        out.segmentNum.min(obj.segmentNumMin);
        out.segmentNum.max(obj.segmentNumMax);
    } else return Nothing;

    if ('atPosR' in obj && typeof obj.atPosR === 'number'
        && 'atPosC' in obj && typeof obj.atPosC === 'number') {
        out.atPos.r(obj.atPosR);
        out.atPos.c(obj.atPosC);
    } else return Nothing;

    if ('repeatRangeMin' in obj && typeof obj.repeatRangeMin === 'number'
        && 'repeatRangeMax' in obj && typeof obj.repeatRangeMax === 'number') {
        out.repeatRange.min(obj.repeatRangeMin);
        out.repeatRange.max(obj.repeatRangeMax);
    } else return Nothing;

    return Just(out);
}

export function loadObjChoice(obj: object): Maybe<Choice> {
    if ('type' in obj) {
        if (obj.type === 'subregion') {
            let out = new SubregionChoice();
            if ('options' in obj && Array.isArray(obj.options)) {
                for (let o of obj.options) {
                    let ms = loadObjSubregion(o);
                    if (ms === Nothing) return Nothing;
                    if (ms.isJust()) out.options.push(ms.val);
                }
                return Just(out);
            } else return Nothing;
        }
        else if (obj.type === 'corridor') {
            let out = new CorridorChoice();
            if ('options' in obj && Array.isArray(obj.options)) {
                for (let o of obj.options) {
                    let ms = loadObjCorridor(o);
                    if (ms === Nothing) return Nothing;
                    if (ms.isJust()) out.options.push(ms.val);
                }
                return Just(out);
            } else return Nothing;
        }
        else return Nothing;
    } else return Nothing;
}

export function loadObjTile(obj: object): Maybe<Tile> {
    if ('name' in obj && typeof obj.name === 'string'
        && 'svg' in obj && typeof obj.svg === 'string') {
        return Just(new Tile(obj.name, obj.svg));
    } else return Nothing;
}

export function loadObjRegion(obj: object, tiles: Tile[]): Maybe<Region> {
    let out: Region;
    if ('name' in obj && typeof obj.name === 'string') {
        out = new Region(obj.name);
    } else return Nothing;

    if ('tile' in obj && typeof obj.tile === 'string') {
        let mt = tiles.find(t => t.name == obj.tile);
        if (mt === undefined) return Nothing;
        else out.tile(mt);
    } else return Nothing;

    if ('sizeMinR' in obj && typeof obj.sizeMinR === 'number'
        && 'sizeMinC' in obj && typeof obj.sizeMinC === 'number'
        && 'sizeMaxR' in obj && typeof obj.sizeMaxR === 'number'
        && 'sizeMaxC' in obj && typeof obj.sizeMaxC === 'number') {
        out.size.min.r(obj.sizeMinR);
        out.size.min.c(obj.sizeMinC);
        out.size.max.r(obj.sizeMaxR);
        out.size.max.c(obj.sizeMaxC);
    }

    if ('choices' in obj && Array.isArray(obj.choices)) {
        for (let o of obj.choices) {
            let ms = loadObjChoice(o);
            if (ms === Nothing) return Nothing;
            if (ms.isJust()) out.choices.push(ms.val);
        }
        return Just(out);
    } else return Nothing;
}

export function loadTile(t: unknown): Maybe<Tile> {
    if (typeof t !== 'object' || t === null) return Nothing;
    let name: string,
        svg: string,
        scale: {x: number, y: number},
        translate: {x: number, y: number};
    if ('name' in t && typeof t.name === 'string') name = t.name;
    else return Nothing;

    if ('svg' in t && typeof t.svg === 'string') svg = t.svg;
    else return Nothing;

    if ('scale' in t && typeof t.scale === 'object' && t.scale !== null
        && 'x' in t.scale && typeof t.scale.x === 'number'
        && 'y' in t.scale && typeof t.scale.y === 'number') scale = {x: t.scale.x, y: t.scale.y}
    else return Nothing;
        
    if ('translate' in t && typeof t.translate === 'object' && t.translate !== null
        && 'x' in t.translate && typeof t.translate.x === 'number'
        && 'y' in t.translate && typeof t.translate.y === 'number') translate = {x: t.translate.x, y: t.translate.y}
    else return Nothing;

    return Just(new Tile(name, svg, scale, translate));
}