import type * as vm from "viewmodel";

interface EventType<T> {
    _subscribers: ((event: T) => void)[];
    subscribe(handler: (event: T) => void): void;
    unsubscribe(handler: (event: T) => void): void;
    pub(event: T): void;
}

export function registerEvent<T>(): EventType<T> {
    return {
        _subscribers: [],
        subscribe(this: EventType<T>, h) {this._subscribers.push(h)},
        unsubscribe(this: EventType<T>, handler) {this._subscribers = this._subscribers.filter((h) => h !== handler)},
        pub(this: EventType<T>, e) {this._subscribers.forEach(f => f(e))}
    }
}

interface ISubregionOptionEvent {
    choice: vm.SubregionChoice;
    option: vm.Subregion
}

interface ICorridorOptionEvent {
    choice: vm.CorridorChoice;
    option: vm.Corridor
}

interface INetFailedEvent {
    method: 'get' | 'post' | 'delete',
    resource: string,
    reason: string
}

interface IGenerateEvent {
    region: vm.Region;
    limit: number
}

export const newRegion = registerEvent<vm.Region>();
export const delRegion = registerEvent<vm.Region>();
export const editRegion = registerEvent<vm.Region>();
export const generateDungeon = registerEvent<IGenerateEvent>();
export const importRegionStore = registerEvent<string>();
export const importIconSet = registerEvent<string>();
export const importRegionReceived = registerEvent<vm.Region>();
export const exportRegionStore = registerEvent<vm.Region>();
export const deleteRegionStore = registerEvent<vm.Region>();
export const netFailed = registerEvent<INetFailedEvent>();
export const selectTile = registerEvent<vm.Tile>();
export const addSubregionChoice = registerEvent<vm.SubregionChoice>();
export const addCorridorChoice = registerEvent<vm.CorridorChoice>();
export const delChoice = registerEvent<vm.Choice>();
export const addSubregion = registerEvent<ISubregionOptionEvent>();
export const addCorridor = registerEvent<ICorridorOptionEvent>();
export const delSubregion = registerEvent<ISubregionOptionEvent>();
export const delCorridor = registerEvent<ICorridorOptionEvent>();
export const zoomIn = registerEvent<number>();
export const zoomOut = registerEvent<number>();