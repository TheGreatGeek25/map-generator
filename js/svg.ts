import * as viewmodel from "viewmodel"; 

export function clearMap(): void {
    let svg = svgElem();
    while (svg.lastChild) {
        svg.removeChild(svg.lastChild);
    }   
}

function svgElem(): HTMLElement & SVGSVGElement {
    return document.getElementById("mapsvg")! as HTMLElement & SVGSVGElement;
}

function createSVG(tag: string): SVGElement {
    return document.createElementNS("http://www.w3.org/2000/svg", tag);
}

function drawCellBorder(x: number, y: number): void {
    let cb = createSVG("rect");
    cb.setAttribute("width", "1");
    cb.setAttribute("height", "1");
    cb.setAttribute("x", (x-0.5).toString());
    cb.setAttribute("y", (y-0.5).toString());
    cb.style.fill = "transparent";
    cb.style.stroke = "black";
    cb.style.strokeWidth = "0.1";

    svgElem().append(cb);
}

function drawTileAt(tile: viewmodel.Tile, x: number, y: number): void {
    let g = createSVG('g') as SVGGElement;
    let trans = svgElem().createSVGTransform();
    trans.setTranslate(x, y);
    g.transform.baseVal.appendItem(trans);
    for (let c of document.getElementById(`tiles_${tile.name}`)!.children) {
        g.append(c.cloneNode(true));
    }
    svgElem().append(g);
}

export function drawTiles(map: viewmodel.TileMap): void {
    for (let x in map) {
        for (let y in map[x]) {
            drawCellBorder(Number(x), Number(y));
            drawTileAt(map[x][y], Number(x), Number(y));
        }
    }
}
