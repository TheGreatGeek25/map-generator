import * as ko from "knockout";
import * as rand from "random";
import { Tile, TileMap, RCCoords, unwrapRC, Region, Subregion, Corridor, RegionMap, ObsInterval, blankTile, SubregionChoice, CorridorChoice } from "viewmodel";

type Bounds = {pos: RCCoords, dim: RCCoords}
const mapBounds: Bounds = {pos: {r:-50, c:-50}, dim: {r:100, c:100}}
const dirs = [{r:0,c:-1},{r:-1,c:0},{r:0,c:1},{r:1,c:0}];

export function generate(regions: RegionMap, map: TileMap, startRegion: Region, origin: RCCoords, bounds: Bounds): rand.Generator<TileMap> {
    let max = findActualMaxDimension(unwrapRC(startRegion.size.max), origin, bounds);

    return rand.generatorChooseArr([...range(ko.unwrap(startRegion.size.min.r), max.r+1)])
        .next(rows => rand.generatorChooseArr([...range(ko.unwrap(startRegion.size.min.c), max.c+1)])
            .next(cols => rand.generatorPure({r: rows, c: cols})))
        .next((dim) => {
            let {r:rows, c:cols} = dim,
                genRange = getBounds(origin, dim);

            for (let r = genRange.pos.r; r < genRange.pos.r+genRange.dim.r; r++) {
                for (let c = genRange.pos.c; c < genRange.pos.c+genRange.dim.c; c++) {
                    if (isTileNonempty(map, r,c)) {
                        return rand.generatorFail;
                    }
                }
            }
            if (!isPlacementValid(map, origin, {r:rows, c:cols}, bounds)) {
                return rand.generatorFail;
            }
            
            return startRegion.choices()
                .filter((c): c is SubregionChoice => c instanceof SubregionChoice)
                .map<(nextmap: TileMap) => rand.Generator<TileMap>>(({options}) => {
                    return (nextmap: TileMap) => rand.generatorChooseWeighted(options()).next((mo) =>
                        mo.maybe((o) => makeSubregionChoice(regions, nextmap, o, origin, getBounds(origin, {r:rows, c:cols})), rand.generatorPure(nextmap)));
                }).reduce<rand.Generator<TileMap>>((acc, ex) => acc.next(ex), rand.generatorPure(map))
                .next((nm) => rand.generatorPure({r: rows, c: cols, nm}))
        }).next(({r: rows, c: cols, nm}) => {
            let genRange = getBounds(origin, {r: rows, c: cols});
            let newmap = placeTilesRect(startRegion.tile(), nm, genRange);

            return startRegion.choices()
                .filter((c): c is CorridorChoice => c instanceof CorridorChoice)
                .map<(tm: TileMap) => rand.Generator<TileMap>>(({options}) => {
                    return (tm) => rand.generatorChooseWeighted(options()).next((mo) => 
                        mo.maybe((o) => makeCorridorChoice(regions, tm, o, origin, genRange), rand.generatorPure(tm)));
                    })
                .reduce((acc, ex) => acc.next(ex), rand.generatorPure(newmap));
        });
}

function getBounds(pos: RCCoords, {r, c}: RCCoords): Bounds {
    return {pos: {r: Math.ceil(pos.r-r/2),
                  c: Math.ceil(pos.c-r/2)},
            dim: {r,c}}
}

function makeSubregionChoice(regions: RegionMap, map: TileMap, subregion: Subregion, origin: RCCoords, genRange: Bounds): rand.Generator<TileMap> {
    let mindim = unwrapRC(regions[subregion.region()].size.min);

    return rand.generatorChooseArr([...range(subregion.repeatRange.min(), subregion.repeatRange.max()+1)]).next(repeat => 
        [...range(0, repeat)]
            .map((_) => {
                let posRange: {r: number[], c: number[]};
                if (subregion.placement() == 'at') {
                    posRange = {r: [subregion.atPos.r() + origin.r],
                                c: [subregion.atPos.c() + origin.c]};
                } else if (subregion.placement() == 'randomly') {
                    let bounds = getRectBounds(genRange, mindim);
                    posRange = {r: [...range(bounds.pos.r, bounds.dim.r+1)], 
                                c: [...range(bounds.pos.c, bounds.dim.c+1)]}
                }
                return (nextmap: TileMap) => rand.generatorChooseArr(posRange.r).next((r) => 
                    rand.generatorChooseArr(posRange.c).next((c) => 
                        isPlacementValid(map, {r,c}, mindim, genRange) 
                            ? generate(regions, nextmap, regions[subregion.region()], {r,c}, genRange) 
                            : rand.generatorFail));
            }).reduce((acc, ex) => acc.next(ex), rand.generatorPure(map))
    )
}

function makeCorridorChoice(regions: RegionMap, map: TileMap, corridor: Corridor, origin: RCCoords, genRange: Bounds): rand.Generator<TileMap> {
    return rand.generatorChooseArr([...range(corridor.repeatRange.min(), corridor.repeatRange.max()+1)]).next((repeat) => 
        [...range(0,repeat)]
            .map((_) => {
                let positions: RCCoords[];
                if (corridor.placement() === 'at') {
                    positions = [{r: origin.r + corridor.atPos.r(), c: origin.c + corridor.atPos.c()}];
                } else if (corridor.placement() === 'randomly') {
                    positions = getEdgePlaces(genRange);
                }

                return (nextmap: TileMap) => rand.generatorChooseArr(positions)
                    .next((pos) => generate(regions, nextmap, regions[corridor.doorRegion()], pos, mapBounds).next((nm) => rand.generatorPure({pos, nm})))
                    .next(({pos, nm}) => {
                        let dir = getDirection(genRange, pos);
                        return rand.generatorChooseArr([...range(corridor.segmentNum.min(), corridor.segmentNum.max()+1)])
                            .next((sNum) => placeCorridor(nm, {r: pos.r+dirs[dir].r, c: pos.c+dirs[dir].c}, dir, sNum, corridor.segmentLen, (pos, nm) =>
                                generate(regions, nm, regions[corridor.endingRegion()], pos, mapBounds)))
                    })
            }).reduce((acc, ex) => acc.next(ex), rand.generatorPure(map))
    );
}

function placeCorridor(map: TileMap, pos: RCCoords, dir: 0|1|2|3, sNum: number, lenBounds: ObsInterval<number>, endRegion: (rc: RCCoords, nm: TileMap) => rand.Generator<TileMap>): rand.Generator<TileMap> {
    if (sNum == 0) {
        return endRegion(pos, map);
    }

    let {r,c} = pos,
        maxLen = 0;
    while (maxLen < lenBounds.max()) {
        if (!(c in map)) {map[c] = {};}
        if (isTileNonempty(map, r, c)) { break; }
        
        r += dirs[dir].r; c += dirs[dir].c;
        maxLen++;
    }

    return rand.generatorChooseArr([...range(lenBounds.min(), maxLen+1)])
        .next((len) => {
            let next: (0|1|2|3)[] = [0,1,2,3];
            rand.removeFromArr(next, (dir+2)%4);
            let dest = {r: pos.r+dirs[dir].r*len, c: pos.c+dirs[dir].c*len};
            return rand.generatorChooseArr(next).next((d) => placeCorridor(map, dest, d, sNum-1, lenBounds, endRegion)).next((nm) => rand.generatorPure({nm,len}))
        }).next(({nm,len}) => {
            let destR = pos.r + dirs[dir].r*len,
                destC = pos.c + dirs[dir].c*len,
                genRange: Bounds = {pos: {r: Math.min(destR-dirs[dir].r, pos.r),
                                          c: Math.min(destC-dirs[dir].c, pos.c)},
                                    dim: {r: Math.max(Math.abs(dirs[dir].r*len), 1),
                                          c: Math.max(Math.abs(dirs[dir].c*len), 1)}};
        
            let newmap = placeTilesRect(blankTile, nm, genRange);
            return rand.generatorPure(newmap)
        });
}

function getDirection(genRange: Bounds, {r,c}: RCCoords): 0 | 1 | 2 | 3 {
    if (c == genRange.pos.c-1) {return 0;}
    if (c == genRange.pos.c+genRange.dim.c) {return 2};

    if (r == genRange.pos.r-1) {return 1;}
    if (r == genRange.pos.r+genRange.dim.r) {return 3;};
    
    throw (new Error("Direction undefined"));
}

function getEdgePlaces(genRange: Bounds): RCCoords[] {
    let places: RCCoords[] = [];
    for (let i=0; i < genRange.dim.r; i++) {
        places.push({r: genRange.pos.r+i, c: genRange.pos.c-1},
                    {r: genRange.pos.r+i, c: genRange.pos.c+genRange.dim.c});
    }

    for (let i=0; i < genRange.dim.c; i++) {
        places.push({r: genRange.pos.r-1, c: genRange.pos.c+i},
                    {r: genRange.pos.r+genRange.dim.r, c: genRange.pos.c+i});
    }
    return places;
}

function getRectBounds(genRange: Bounds, mindim: RCCoords): Bounds {
    return {pos: {r: Math.floor(genRange.pos.r + mindim.r/2),
                  c: Math.floor(genRange.pos.c + mindim.c/2)},
            dim: {r: (genRange.pos.r+genRange.dim.r-1) - Math.floor(mindim.r/2-0.5),
                  c: (genRange.pos.c+genRange.dim.c-1) - Math.floor(mindim.c/2-0.5)}};
}

function placeTilesRect(tile: Tile, map: TileMap, genRange: Bounds): TileMap {
    let {pos: {r, c}, dim: {r: rows, c: cols}} = genRange;
    let newmap = copyMap(map);
    for (let i = r; i < r+rows; i++) {
        for (let j = c; j < c+cols; j++) {
            if (isTileNonempty(newmap, i, j)) {
                //console.log("Collision at (r,c): " + i + "," + j);
            } else {
                if (!(j in newmap)) {
                    newmap[j] = {};
                }
                newmap[j][i] = tile;
            }
        }
    }
    return newmap;
}

function copyMap(map: TileMap): TileMap {
    let newmap: TileMap = {};
    for (let c in map) {
        newmap[c] = {};
        for (let r in map[c]) {
            newmap[c][r] = map[c][r]
        }
    }
    return newmap;
}

function findActualMaxDimension(dim: RCCoords, pos: RCCoords, genRange: Bounds): RCCoords {
    let maxR = Math.min(dim.r,
                        (pos.r - genRange.pos.r)*2 + 1,
                        (genRange.pos.r+genRange.dim.r-1 - pos.r)*2 + 1);
    let maxC = Math.min(dim.c,
                        (pos.c - genRange.pos.c)*2 + 1,
                        (genRange.pos.c+genRange.dim.c-1 - pos.c)*2 + 1);
    return {r: maxR, c: maxC};
}


function isTileNonempty(map: TileMap, r: number, c: number): boolean {
    return c in map && r in map[c];
}

function isPlacementValid(map: TileMap, pos: RCCoords, dim: RCCoords, genRange: Bounds): boolean {
    let r = Math.ceil(pos.r - dim.r/2);
    let c = Math.ceil(pos.c - dim.c/2);
    for (let i=r; i < r+dim.r; i++) {
        for (let j=c; j < c+dim.c; j++) {
            if (isTileNonempty(map, i, j)
                || i < genRange.pos.r || i > genRange.pos.r+genRange.dim.r-1
                || j < genRange.pos.c || j > genRange.pos.c+genRange.dim.c-1) {
                return false;
            }
        }
    }
    return true;
}

function* range(min: number, max: number) {
    let i = min;
    while (i < max) yield i++;
    return max;
}
